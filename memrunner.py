# memrunner
#
# TODO: Add parser.add_argument('--unit', default='B', choices=[
#            'B', 'KiB', 'MiB', 'GiB', 'KB', 'MB', 'GB'])

import argparse
import os
import psutil
import subprocess
import sys
import time


def _monitor_pid(run_pid: int, mon_pid: int, f, args):
    try:
        run_p = psutil.Process(run_pid)
    except psutil.NoSuchProcess:
        unused_child_pid, status = os.waitpid(mon_pid, 0)
        return 0, status

    max_value = {}

    while True:
        rss_list = []
        try:
            rss_list.append(run_p.memory_info().rss)
        except psutil.NoSuchProcess:
            pass
        if args.recursive:
            for child_p in run_p.children(recursive=True):
                try:
                    rss_list.append(child_p.memory_info().rss)
                except psutil.NoSuchProcess:
                    pass

        values = {}
        for mode in args.aggregation_mode:
            if mode == 'max':
                values[mode] = max(rss_list)
            elif mode == 'sum':
                values[mode] = sum(rss_list)

        output = []
        for mode in args.aggregation_mode:
            value = values[mode]
            unit = '{}_rss_bytes'.format(mode)
            output.append('{}: {}'.format(unit, value))

            if args.print_max_value:
                if max_value.get(mode) is not None:
                    max_value[mode] = max(value, max_value[mode])
                else:
                    max_value[mode] = value
                output.append('max({}): {}'.format(unit, max_value[mode]))

        if args.print_num_children:
            output.append('num_child_processes: {}'.format(len(rss_list)))

        print(' '.join(output), file=f, flush=True)

        child_pid, status = os.waitpid(mon_pid, os.WNOHANG)
        if child_pid > 0:
            break
        time.sleep(args.t)

    return child_pid, status


def main():
    parser = argparse.ArgumentParser(
        description=(
            'Runs give cmd with args and measures RSS (memory usage) '
            'periodically. '
            '"--" should be added before cmd. '
            ),
        usage='memrunner.py [-t SECS] -- cmd args ...')
    parser.add_argument(
        '--aggregation-mode', default=[], action='append',
        choices=['sum', 'max'])
    parser.add_argument('cmdargs', nargs='+')
    parser.add_argument(
        '--print-max-value', action='store_true',
        help='Print the maximum value observed during execution')
    parser.add_argument(
        '--print-num-children', action='store_true',
        help=('Print the number of child processes that are used to calculate '
              'the statistics. Note: --max-recursive argument affects this.'))
    parser.add_argument(
        '--recursive', action='store_true',
        help=('Look at all child processes. E.g. the '
              'sum of RSS memory sizes among child processes.'))
    parser.add_argument(
        '-t', metavar='SECS', default=1.0, type=float,
        help='Measurement period in seconds (float)')
    parser.add_argument(
        '-w', metavar='FILE', help='Write statistics to FILE')
    args = parser.parse_args()

    if len(args.aggregation_mode) == 0:
        args.aggregation_mode.append('sum')

    pipe_r, pipe_w = os.pipe()

    mon_pid = os.fork()
    if mon_pid == 0:
        os.close(pipe_r)
        proc = subprocess.Popen(args.cmdargs)
        pid_bytes = str(proc.pid).encode()
        wlen = os.write(pipe_w, pid_bytes)
        assert wlen == len(pid_bytes)
        returncode = proc.wait()
        sys.exit(returncode)
    else:
        os.close(pipe_w)
        run_pid_bytes = os.read(pipe_r, 32)
        run_pid = int(run_pid_bytes.decode())

        f = sys.stderr
        if args.w is not None:
            f = open(args.w, 'w')

        child_pid, status = _monitor_pid(run_pid, mon_pid, f, args)

        if child_pid == 0:
            print('There was no time to measure the child process.')
        else:
            assert child_pid == mon_pid

        if os.WIFEXITED(status):
            returncode = os.WEXITSTATUS(status)
        elif os.WIFSIGNALED(status):
            # NOT TESTED
            returncode = -os.WTERMSIG(status)
        else:
            raise ValueError('Bad status: {}'.format(status))
        sys.exit(returncode)


if __name__ == '__main__':
    main()
